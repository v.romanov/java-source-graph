#### `node_names_all.csv` contains export of names of all nodes

```
cat node_names_all.csv | wc -l                                          
  475983
```

```
cat node_names_all.csv | awk -F"," '{print $2}' | sort | uniq -c | wc -l 
  182375
```

#### `node_names_all.csv` contains export of names that occur at least two times

```
cat node_names.csv | wc -l                                          
  358724
```

```
cat node_names.csv | awk -F"," '{print $2}' | sort | uniq -c | wc -l 
  65122
```